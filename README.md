# TODO:
- [ ] buffer scroll position between database update on "inspect" view;
- [ ] purge input value of description between editions on different sets on "sets" view;
- [ ] prevent editions during database transaction on "sets" view;
- [ ] allow batch edition on tags for multiple gifs.

# License
![wtpfl](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png) - [wtpfl.net](http://www.wtfpl.net/)
