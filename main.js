const { app, BrowserWindow } = require('electron');

const { join } = require('path');
const { format } = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let main_window;

const create_window = () => {
  // Create the browser window.
  main_window = new BrowserWindow({
    width: 1950,
    height: 1080,
    show: false
  })

  // and load the index.html of the app.
  main_window.loadURL(format({
    pathname: join(__dirname, 'build/es6-unbundled/index.html'),
    protocol: 'file:',
    slashes: true
  }))

  main_window.once('ready-to-show', () => {
    main_window.show()
    main_window.webContents.openDevTools()
  })

  // Emitted when the window is closed.
  main_window.once('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    main_window = null
  })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', create_window)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (main_window === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
