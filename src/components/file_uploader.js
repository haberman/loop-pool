import { LitElement, html, css } from 'lit-element'

import { host, list, link } from '../styles/common'
import { fa } from '../styles/font_awesome'

import * as api from '../data/api'
import { timeOut } from '../fn';

export class FileUploader extends LitElement {
    static get styles() {
        return [
            host, list, link, fa,
            css`
            #upload_list {
                margin-bottom: 10px;
            }

            #upload_list li {
                height: 25px;
                margin-bottom: 5px;
            }

            #upload_dropout {
                background-color: var(--color-overlay-black);
                text-align: center;
                transition: background-color var(--transition-duration-fast) ease-in-out;
            }

            #upload_dropout.dragged-over
            {
                background-color: var(--color-ultralight);
            }

            #upload_dropout >a {
                cursor: pointer;
                margin: 20px;
            }

            #upload_dropout.dragged-over >a {
                color: var(--color-accent-blue);
            }

            .upload-item {
                position: relative;
                height: 26px;
                background-color: var(--color-ultralight);
            }

            .upload-info {
                position: absolute;
                width: 100%;
            }

            .upload-info p {
                max-width: 75%;
                display: inline-block;
                padding: 5px 5px 4px 5px;
                margin: 3px 0 2px;
                background-color: var(--color-overlay-black-strong);
                color: white;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: bold;
            }

            .upload-info span {
                float: right;
                margin: 1px 1px 1px;
                padding: 7px 7px 6px 7px;
                background-color: white;
                transition: width, height var(--transtion-duration-fast) ease-in-out;
            }

            .upload-progress {
                position: absolute;
                width: 0%;
                top: 0;
                bottom: 0;
                left: 0;
                background: var(--color-overlay-black-light)  url(../../img/loading.gif) repeat;
                transition: width var(--transition-duration-fast) ease-in-out;
            }

            i.fa {
                margin-top: -1px;
            }

            i.hidden {
                display: none;
            }`
        ]
    }

    static get properties() {
        return {
            files: { type: Array },
            type: { type: Array }
        }
    }

    constructor() {
        super()
        this.files = []
        this.type = []
    }

    connectedCallback() {
        super.connectedCallback()
        this.addEventListener('upload-progress', this._on_upload_progress)
    }


    _filter_submitted_files(files) {
        for (const file of files) {
            if (this.type && this.type.includes(file.type)) { this._add_file_to_upload(file) }
        }
    }

    _file_index_from_file_name(name) {
        return this.files.findIndex(file => file.name === name)
    }

    _add_file_to_upload(file) {
        api
            .read({
                column: 'src',
                value: file.name
            }, 'gif')
            .then((data) => {
                if (data.data === false) {
                    api.upload_gif(file, this)
                        .then(data => {
                            if (data.code === 200) { this._on_upload_success(data.data) }
                            else { this._on_upload_error(data) }
                        })

                    this.files.push({
                        name: file.name,
                        path: file.path,
                        size: file.size,
                        state: {
                            status: 'uploading',
                            info: 0
                        }
                    })

                    this.requestUpdate('files')
                }
            })
    }


    _on_upload_progress(e) {
        const index = this._file_index_from_file_name(e.detail.file_name)
        this.files[index].state = { status: 'uploading', info: e.detail.progress }
        this.requestUpdate('files')
    }

    _on_upload_success(data) {
        const index = this._file_index_from_file_name(data.file)
        this.files.splice(index, 1)
        this.requestUpdate('files')
        this.dispatchEvent(new CustomEvent('file-upload', { bubbles: false }))
    }

    _on_upload_error(data) {
        const index = this._file_index_from_file_name(data.file)
        this.files[index].state = { status: 'error', info: data.info }
        this.requestUpdate('files')

        const that = this
        timeOut.after(2000).run(() => {
            that.files.splice(index, 1)
            that.requestUpdate('files')
        })
    }

    _dragged_start() {
        this.shadowRoot.getElementById('upload_dropout').classList.add('dragged-over')
    }

    _dragged_end() {
        this.shadowRoot.getElementById('upload_dropout').classList.remove('dragged-over')
    }

    _on_mouseover(e) {
        e.stopPropagation()
        this._dragged_start()
    }

    _on_mouseout(e) {
        e.stopPropagation()
        this._dragged_end()
    }

    _on_click(e) {
        e.preventDefault()
        this.shadowRoot.getElementById('upload_input').click()
    }

    _on_dragover(e) {
        e.preventDefault()
        this._dragged_start()

        return false
    }

    _on_dragleave(e) {
        e.preventDefault()
        this._dragged_end()

        return false
    }

    _on_drop(e) {
        e.preventDefault()
        this._filter_submitted_files(e.dataTransfer.files)

        return false
    }

    _on_input(e) {
        this._filter_submitted_files(e.target.files)
    }

    // _on_slider_click(e) {
    //     e.stopPropagation()

    //     const oldval = this.getAttribute('on') === ''
    //     this.setAttribute('on', oldval ? null : '')

    //     this.requestUpdate()
    // }

    _content_percent(state) {
        let percent = ''

        switch (state.status) {
            case 'uploading':
                percent = state.info < 1 ? `${Math.round(state.info * 100)}%` : ''
                break
            case 'error':
                percent = ''
                break
            default:
                percent = '0%'
                break
        }

        return percent
    }

    _content_info(file, state) {
        let info = ''

        switch (state.status) {
            case 'uploading':
                info = `${file.name} (${(file.size * .001).toFixed(2)} kb)`
                break

            case 'error':
                info = state.info
                break;
        }

        return info
    }

    _style_item(state) {
        if (state.status === 'error') { return 'background-color:var(--color-accent-red);' }
        return ''
    }

    _style_progress(state) {
        let style = 'width:0%;'

        switch (state.status) {
            case 'uploading':
                if (state.info < 1) { style = `width:${state.info * 100}%;` }
                else { style += 'width:100%;background-color:var(--color-accent-blue);' }
                break
            case 'uploaded':
                console.log('file upload is done, should remove dom item');
                break
        }

        return style
    }

    _class_spinner(state) {
        if (state.status === 'uploading' && state.info === 1) { return 'fa fa-refresh fa-spin fa-fw' }
        else if (state.status === 'error') { return 'fa fa-exclamation-triangle' }

        return 'hidden'
    }

    render() {
        return html`
        <ul id='upload_list'>
        ${this.files.map(file =>
            html`
            <li>
                <div class='upload-item' style='${this._style_item(file.state)}'>
                    <div class='upload-progress' style='${this._style_progress(file.state)}'></div>
                    <div class='upload-info'>
                        <span>${this._content_percent(file.state)}
                            <i class='${this._class_spinner(file.state)}' aria-hidden='true'></i>
                        </span>
                        <p>${this._content_info(file, file.state)}</p>
                    </div>
                </div>
            </li>`)}
        </ul>
        <div id='upload_dropout' @dragover='${this._on_dragover}'
            @dragend='${this._on_dragend}' @dragleave='${this._on_dragleave}' @drop='${this._on_drop}'>
            <input type='file' id='upload_input' style='display:none' multiple @input=${this._on_input}>
            <a href='#' class='fa fa-3x fa-upload' aria-hidden='true'
                @mouseover='${this._on_mouseover}' @mouseout='${this._on_mouseout}' @click=${this._on_click}></a>
        </div>`
    }
}
customElements.define('file-uploader', FileUploader)