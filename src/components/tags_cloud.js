import { LitElement, html, css } from 'lit-element'

import { host, title, link, list, input } from '../styles/common'
import { fa } from '../styles/font_awesome'

export class TagsCloud extends LitElement {
    static get styles() {
        return [
            host, title, link, list, input, fa,
            css`
            h3 input[type=checkbox] {
                float: left;
                margin: -1px 6px 0 0;
            }

            h3 ul {
                font-weight: lighter;
                float: right;
                margin-right: 1.5em;
            }

            h3 ul li {
                display: inline-block;
            }

            h3 ul li a:hover {
                text-decoration: underline;
            }

            .tags-items li {
                display: inline-block;
                margin: 0 0 5px 0;
                padding: 0;
            }

            .tags-items li a {
                position: relative;
                display: inline-block;
                padding: 5px 0 3px 5px;
                color: var(--color-light);
                background-color: white;
                border: 1px solid var(--color-ultralight);
                transition: color, background-color, border-color var(--transition-duration-fast) ease-in-out;
            }

            .tags-items li span {
                margin-left: 5px;
                padding: 5px 5px 3px;
                color: var(--color-normal);
                background-color: var(--color-ultralight);
                text-align: center;
                transition: color, background-color, border-color var(--transition-duration-fast) ease-in-out;
            }

            .tags-items li a:hover, ul li.selected a:hover {
                background-color: var(--color-light);
                border-color: var(--color-light);
                color: white;
            }

            .tags-items li a:hover span, ul li.selected a:hover span {
                background-color: var(--color-background-hover);
                color: white;
            }

            .tags-items li.selected a {
                color: white;
                background-color: var(--color-dark);
                border-color : black;
            }

            .tags-items li.selected span {
                color: black;
                background-color: white;
            }

            #mode_switch {
                margin-top: 1em;
            }`
        ]
    }

    static get properties() {
        return {
            heading: { type: String },
            icon: { type: String },
            tags: { type: Array },
            exclusive: { type: Boolean },
            active: { type: Boolean }
        }
    }

    constructor() {
        super()
        this.heading = ''
        this.icon = ''
        this.tags = []
        this.exclusive = false
        this.active = true
    }

    _tags_select(selected) {
        for (let i = 0; i < this.tags.length; ++i) { this.tags[i].selected = selected }
        this._dispatch_selection_change()
    }

    _on_exclusivity_switch(e) {
        this.exclusive = e.detail
        this._dispatch_selection_change()
    }

    _on_active_click(e) {
        this.active = e.currentTarget.checked
        
        if (!this.active) {
            this.dispatchEvent(new CustomEvent('active-change', {
                bubbles: false,
                detail: { active: false }
            }))
        } else { this._dispatch_selection_change() }
    }

    _on_tag_click(tag_index) {
        this.tags[tag_index].selected = !this.tags[tag_index].selected
        this._dispatch_selection_change()
    }

    _dispatch_selection_change() {
        const selected_tags = this.tags.filter((tag) => tag.selected)

        this.dispatchEvent(new CustomEvent('selection-change', {
            bubbles: false,
            detail: {
                tags: selected_tags,
                exclusive: this.exclusive
            }
        }))

        this.requestUpdate('tags')
    }

    render() {
        return html`
        <h3>
            <input id='active_checkbox' type='checkbox' ?checked=${this.active} @click=${this._on_active_click}>
            <i class='fa fa-${this.icon}' aria-hidden='true'></i>${this.heading}
            <ul>
                <li><a href='#' @click=${() => this._tags_select(true)}>all</a></li>
                <li><a href='#' @click=${() => this._tags_select(false)}>none</a></li>
            </ul>
        </h3>
        ${this.active ? html`
        <ul class='tags-items'>
        ${this.tags.map((tag, idx) =>
            html`
            <li class=${tag.selected ? 'selected' : ''}>
                <a href='#${tag.gw_tag_id}' @click=${() => this._on_tag_click(idx)}>
                    ${tag.gw_tag_name}<span>${tag.gw_tag_count}</span>
                </a>
            </li>`
        )}
        </ul>
        <switch-button id='mode_switch' hint='EXCLUSIVE' .on='${this.exclusive}' @switched=${this._on_exclusivity_switch}></switch-button>` : ''}`
    }
}
customElements.define('tags-cloud', TagsCloud)