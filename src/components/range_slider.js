import { LitElement, html, css } from 'lit-element'
import { styleMap } from 'lit-html/directives/style-map'

import { host, title } from '../styles/common'
import { fa } from '../styles/font_awesome'

const HANDLE = { 'none': -1, 'left': 0, 'right': 1, 'range': 2 }

export class RangeSlider extends LitElement {
    static get styles() {
        return [
            host, title, fa,
            css`
            h3 span {
                font-size: var(--font-size-small);
                font-weight: lighter;
                float: right;
                margin-right: 1.5em;
            }

            h3 input[type=checkbox] {
                float: left;
                margin: -1px 6px 0 0;
            }

            #slider_container {
                position: relative;
                height: 21px;
                background-color: var(--color-ultralight);
            }

            #slider_range {
                position: absolute;
                top: 0px;
                height: 100%;
                background: var(--color-overlay-black-light)  url(../../img/stripes.png) repeat;
                border-left: 1px solid var(--color-overlay-black-strong);
                border-right: 1px solid var(--color-overlay-black-strong);
                cursor: move;
                transition: background-color, border-color var(--transition-duration-fast) ease-in-out;
            }

            #slider_range:hover {
                background-color: var(--color-overlay-white-light);
                border-color: black
            }

            #slider_range .handle {
                position: absolute;
                height: var(--size-handle);
                width: var(--size-handle);
                background-color: var(--color-ultralight);
                border: 1px solid var(--color-normal);
                border-radius: 100%;
            }

            #slider_range .handle:hover {
                background-color: white;
                border-color: black;
            }

            #handle_left {
                left: calc(-.5 * var(--size-handle) - 2px);
                cursor: w-resize;
            }

            #handle_right {
                right: calc(-.5 * var(--size-handle) - 1px);
                cursor: e-resize;
            }

            #handle_left, #handle_right {
                top: 50%;
                margin-top: calc(-.5 * var(--size-handle) - 1px);
            }`
        ]
    }

    static get properties() {
        return {
            active: { type: Boolean },
            heading: { type: String },
            icon: { type: String },
            limits: { type: Object },
            range: { type: Object, attribute: false },
            range_format: { type: Function, attribute: false },
            range_css: { type: Object }
        }
    }

    constructor() {
        super()

        this.active = false
        this.heading = ''
        this.icon = ''
        this.limits = {}
        this.range = {}
        this.range_format = n => n
        this.range_css = { left: '0%', right: '0%', display: 'none' }

        this._handle_active = HANDLE.none
        this._handle_left = 0
        this._handle_right = 0
    }

    connectedCallback() {
        super.connectedCallback()

        this._binded_mouse_up = this._on_mouse_up.bind(this)
        this._binded_mouse_move = this._on_mouse_move.bind(this)
    }

    firstUpdated() {
        if (this.active) {
            this._measure_ui()
        }
    }

    attributeChangedCallback(name, oldval, newval) {
        if (name == 'limits') {
            this.range_css.display = null
            this.range = JSON.parse(newval)

            this.requestUpdate()
        }
        super.attributeChangedCallback(name, oldval, newval);
    }

    _on_checked_click(e) {
        this.active = e.currentTarget.checked

        if (!this.active) {
            this.dispatchEvent(new CustomEvent('active-change', {
                bubbles: false,
                detail: {
                    active: false
                }
            }))
        } else { this._dispatch_range_change() }
    }

    _on_left_handle_down(e) {
        e.stopPropagation()
        this._handle_active = HANDLE.left
        this._start_drag()
    }

    _on_right_handle_down(e) {
        e.stopPropagation()
        this._handle_active = HANDLE.right
        this._start_drag()
    }

    _on_slider_handle_down(e) {
        this._handle_active = HANDLE.range
        this._mouse_origin_x = e.clientX
        this._start_drag()
    }

    _on_mouse_up(e) {
        window.removeEventListener('mouseup', this._binded_mouse_up)
        window.removeEventListener('mousemove', this._binded_mouse_move)

        this._measure_ui()
        this._dispatch_range_change()
    }

    _on_mouse_move(e) {
        const lower_bound = this._range_bounds.left
        const upper_bound = this._range_bounds.right

        // reject on mouse out of limits
        if (e.clientX < lower_bound || e.clientX > upper_bound) { return }

        let style = { left: this._handle_left, right: this._handle_right }
        let mouse_slope = (e.clientX - lower_bound) / this._range_bounds.width

        // snapping to limit's range on low values
        if (mouse_slope < .008) { mouse_slope = 0 }
        else if (mouse_slope > .992) { mouse_slope = 1 }

        switch (this._handle_active) {
            case HANDLE.left:
                style.left = mouse_slope
                break
            case HANDLE.right:
                style.right = 1 - mouse_slope
                break
            case HANDLE.range:
                const slope_origin = (this._mouse_origin_x - lower_bound) / this._range_bounds.width
                const slope_diff = mouse_slope - slope_origin

                style.left = Math.max(0, style.left + slope_diff)
                style.right = Math.max(0, style.right - slope_diff)
                break
        }

        const range_width = this._range_bounds.width - (style.left + style.right) * this._range_bounds.width

        // apply transforms only if slider width is above 20 px
        if (range_width > 20) {
            this.range = {
                min: style.left * (this.limits.max - this.limits.min) + this.limits.min,
                max: Math.abs(style.right - 1) * this.limits.max
            }

            this.range_css.left = `${style.left * 100}%`
            this.range_css.right = `${style.right * 100}%`
        }
    }

    _start_drag() {
        this._measure_ui()

        window.addEventListener('mouseup', this._binded_mouse_up)
        window.addEventListener('mousemove', this._binded_mouse_move)
    }

    _measure_ui() {
        const slider_range = this.shadowRoot.getElementById('slider_range')

        this._handle_left = parseFloat(slider_range.style.left) * .01
        this._handle_right = parseFloat(slider_range.style.right) * .01
        this._range_bounds = this.shadowRoot.getElementById('slider_container').getBoundingClientRect()
    }

    _dispatch_range_change() {
        this.dispatchEvent(new CustomEvent('range-change', {
            detail: {
                min: Math.round(this.range.min),
                max: Math.round(this.range.max)
            }
        }))
    }

    render() {
        return html`
        <h3>
            <input id='active_checkbox' type='checkbox' ?checked=${this.active} @click=${this._on_checked_click}>
            <i class='fa fa-${this.icon}' aria-hidden='true'></i>${this.heading}
            <span>${this.range_format(this.range.min)} - ${this.range_format(this.range.max)}</span>
        </h3>
        ${this.active ? html`
        <div id='slider_container'>
            <div id='slider_range' style='${styleMap(this.range_css)}'
                @mousedown=${this._on_slider_handle_down}>
                <a id='handle_left' class='handle' @mousedown=${this._on_left_handle_down}</a>
                <a id='handle_right' class='handle' @mousedown=${this._on_right_handle_down}></a>
            </div>
        </div>`: ''}`
    }
}
customElements.define('range-slider', RangeSlider)