import { LitElement, html, css } from 'lit-element'

import { host } from '../styles/common'

export class SwitchButton extends LitElement {
    static get styles() {
        return [
            host,
            css`
            .title {
                font-size: 13px;
                display: inline-block;
                vertical-align: middle;
                margin: -15px 0 0 0;
                transition: color, var(--transition-duration-fast) ease-in-out;
            }
            
            .switch {
                position: relative;
                display: inline-block;
                width: var(--size-switch-width);
                height: var(--size-switch-height);
            }
            
            .switch input[type=checkbox] {
                display: none;
            }
            
            .slider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: var(--color-light);
                transition: background-color, var(--transition-duration-fast) ease-in-out;
            }

            .slider.checked {
                background-color: var(--color-dark);
            }
            
            .slider:before {
                position: absolute;
                content: '';
                height: calc(var(--size-switch-height) - 4px);
                width: calc(.5 * var(--size-switch-width) - 2px);
                left: 2px;
                bottom: 2px;
                background-color: white;
                transition: background-color, var(--transition-duration-fast) ease-in-out;
            }
            
            .slider.checked:before {
                transform: translateX(calc(.5 * var(--size-switch-width) - 2px));
            }`
        ]
    }

    static get properties() {
        return {
            hint: { type: String },
            on: { type: Boolean }
        }
    }

    constructor() {
        super()
        this.on = false
        this.hint = ''
    }

    _on_slider_click(e) {
        e.stopPropagation()
        this.on = !this.on
        this.dispatchEvent(new CustomEvent('switched', {
            bubbles: false,
            detail: this.on
        }))
    }

    render() {
        return html`
        <label class='switch'>
            <span class='${this.on ? 'slider checked' : 'slider'}' @click=${this._on_slider_click}></span>
        </label>
        <p class='title' style='${this.on ? '' : 'text-decoration:line-through; color:var(--color-light)'}'>${this.hint}</p>`
    }
}
customElements.define('switch-button', SwitchButton)