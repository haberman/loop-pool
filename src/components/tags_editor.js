import { LitElement, html, css } from 'lit-element'

import { host, link, list, input } from '../styles/common'
import { fa } from '../styles/font_awesome'

import './text_input'

const tag_css = {
    placeholder: {
        'margin': 0,
        'padding': '5px 0 5px 5px',
        'font-size': 'var(--font-size-medium)',
        'background-color': 'var(--color-ultralight)',
        'border': '1px solid black'
    }, input: { 'font-size': 'var(--font-size-medium)' }
}

export class TagsEditor extends LitElement {
    static get styles() {
        return [
            host, link, list, input, fa,
            css`
            li.tag-item {
                display: inline-block;
                margin-bottom: 6px;
            }

            .tag-editor {
                margin: 0;
                padding: 5px 0 3px 5px;
                font-size: var(--font-size-medium);
                background-color: var(--color-ultralight);
                border: 1px solid black;
                cursor: pointer;
            }

            .tag-item a {
                margin-right: 5px;
            }

            .tag-item a:hover {
                color: var(--color-accent-red);
            }

            input {
                font-size: var(--font-size-normal);
            }

            #new_tag_input {
                display: block;
                border-bottom: solid 2px var(--color-dark);
            }`
        ]
    }

    static get properties() {
        return {
            tags: { type: Array },
            string_tags: { type: String }
        }
    }

    constructor() {
        super()
        this.tags = []
        this.string_tags = ''
    }

    _on_tag_click(e) {
        e.stopPropagation()

        this.setAttribute('focused', '')
        this.requestUpdate()
    }

    _on_tag_delete(e) {
        e.stopPropagation()

        const tag_idx = e.currentTarget.parentNode.getAttribute('data-index')
        const tag_del = this.tags[tag_idx]

        this.tags.splice(tag_idx, 1)
        this.requestUpdate('tags')

        this._dispatch_change(tag_del, 'delete')
    }

    _on_input_keydown(e) {
        if (e.keyCode === 13) {
            const tag_value = e.currentTarget.value
            const tag_exists = this.tags.find(tag => tag.name === tag_value)

            if (tag_exists === undefined) {
                const new_tag = { name: tag_value }

                if (this.tags.length > 0) { this.tags.push(new_tag) }
                else { this.tags = [new_tag] }

                this.requestUpdate('tags')
                this._dispatch_change(new_tag, 'write')
            }

            e.currentTarget.blur()
        }
    }

    _on_tag_change(e) {
        const oldval = this.tags
        const tag_idx = e.currentTarget.getAttribute('data-index')

        if (oldval[tag_idx].name == e.detail) { return }

        this.tags[tag_idx].name = e.detail
        this.requestUpdate('tags', oldval)
        this._dispatch_change(this.tags[tag_idx], 'update')
    }

    _on_input_blur(e) { e.currentTarget.value = '' }

    _dispatch_change(tag, action) {
        this.dispatchEvent(new CustomEvent('tags-change', {
            bubbles: false,
            detail: {
                tag: tag,
                action: action
            }
        }))
    }

    render() {
        return html`
        <ul id='tags_list'>
        ${this.tags.map((tag, idx) =>
            html`
            <li class='tag-item'>
                <text-input css='${JSON.stringify(tag_css)}' min_char="2" data-index='${idx}'
                    .value='${tag.name}' @input-change=${this._on_tag_change}">
                    <a href='#' aria-hidden='true' class='fa fa-times' @click=${this._on_tag_delete}></a>
                </text-input>
            </li>`)}
            <li class='tag-item'>
                <input id='new_tag_input' type='text' @blur=${this._on_input_blur} @keydown=${this._on_input_keydown}>
            </li>
        </ul>`
    }
}
customElements.define('tags-editor', TagsEditor)