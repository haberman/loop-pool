import { LitElement, html, css } from 'lit-element'

import { host, list } from '../styles/common'
import { fa } from '../styles/font_awesome'

export class NavMenu extends LitElement {
    static get styles() {
        return [
            host, list, fa,
            css`
            :host {
                font: 12px var(--font-monospace);
            }

            ul {
                overflow: hidden;
                background-color: var(--color-light);
            }

            li {
                background-color: none;
                float: left;
            }

            li.selected, li.selected a:hover {
                background-color: white;
            }

            li a {
                color: white;
                display: block;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                text-transform: uppercase;
                transition: background-color var(--transition-duration-fast) ease-in-out;
            }

            li a:hover {
                background-color: var(--color-overlay-black-strong);
            }

            li.selected a {
                color: var(--color-dark);
            }

            li a > span {
                margin-left: 2px;
            }`
        ]
    }

    static get properties() {
        return { items: { type: Array } }
    }

    constructor() {
        super()
        this.items = []
    }

    _on_item_click(e) {
        const item_idx = e.currentTarget.parentNode.getAttribute('data-index')
        const item = this.items[item_idx]

        for (let i = 0; i < this.items.length; i++) {
            if (i !== item_idx) { this.items[i].selected = false }
        }

        if (item.selected === false) {
            item.selected = true
            this.dispatchEvent(new CustomEvent('nav-change', {
                bubbles: false,
                detail: {
                    target: e.currentTarget,
                    item: item
                }
            }))
        }

        this.requestUpdate('items')
    }

    render() {
        return html`
        <nav id='menu'>
            <ul>
            ${this.items.map((item, idx) =>
                html`
                <li class='${item.selected ? 'selected' : ''}'>
                    <div data-index='${idx}'>
                        <a href='#${item.title}' @click=${this._on_item_click}>
                            <i class='fa fa-${item.icon}' aria-hidden='true'></i>
                            <span>${item.title}</span>
                        </a>
                        ${item.plus ?
                        html`<a href='#add-${item.title}'><i class='fa fa-plus'><i>` :
                        html``}
                    </div>
                </li>`)}
            </ul>
        </nav>`
    }
}
customElements.define('nav-menu', NavMenu)