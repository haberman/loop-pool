import { LitElement, html, css } from 'lit-element'
import { styleMap } from 'lit-html/directives/style-map'

import { host, input } from '../styles/common'

export class TextInput extends LitElement {
    static get styles() {
        return [
            host, input,
            css`
            #placeholder {
                cursor: pointer;
            }

            input {
                border-bottom: solid 2px var(--color-dark);
            }`
        ]
    }

    static get properties() {
        return {
            value: { type: String },
            min_char: { type: Number },
            size: { type: Number },
            focused: { type: Boolean },
            css: { type: Object }
        }
    }

    constructor() {
        super()
        this.value = ''
        this.min_char = this.size = 3
        this.focused = false
        this.css = { placeholder: {}, input: {} }
    }

    connectedCallback() {
        this.size = this.value.length
        this._on_focus_changed(this.focused, false)
        super.connectedCallback()
    }

    attributeChangedCallback(name, oldval, newval) {
        if (name === 'focused') { this._on_focus_changed(newval === '', true) }
        super.attributeChangedCallback(name, oldval, newval);
    }

    async _on_focus_changed(focused, dispatch_change) {
        const dom_input = this.shadowRoot.getElementById('input')

        if (focused) {
            this.css.placeholder.display = 'none'
            this.css.input.display = 'inline-block'

            await this.requestUpdate('css')
            dom_input.focus()
        } else {
            this.css.placeholder.display = 'inline-block'
            this.css.input.display = 'none'

            if (dispatch_change) {
                const newval = dom_input.value

                if (newval !== this.value) {
                    this.value = newval
                    this.dispatchEvent(new CustomEvent('input-change', { detail: this.value }))
                }
            }
        }
    }

    _on_placeholder_click(e) {
        this.setAttribute('focused', '')
        this.requestUpdate('focused')
    }

    _on_input_keydown(e) {
        if (!this.focused) { return false }
        if (e.keyCode === 13 || e.keyCode === 27) { e.currentTarget.blur() }
    }

    _on_input_focus(e) {
        e.currentTarget.selectionEnd = this.value.length
    }

    _on_input_blur() {
        if (this.value.length < this.min_char) { return false } // maybe return a CustomEvent for bad value

        this.setAttribute('focused', null)
        this.requestUpdate('focused')
    }

    _on_input(e) {
        const newval = e.currentTarget.value.length

        if (newval >= this.min_char) {
            const oldval = this.size
            this.size = e.currentTarget.value.length
            this.requestUpdate('size', oldval)
        }
    }

    render() {
        return html`
        
        <div id='placeholder' style='${styleMap(this.css.placeholder)}'
            @click=${this._on_placeholder_click}>
            <span>${this.value}</span>
            <slot></slot>
        </div>
        <input id='input' type='text' style='${styleMap(this.css.input)}'
            .size='${this.size}' .value='${this.value}'
            @focus=${this._on_input_focus} @blur=${this._on_input_blur}
            @keydown=${this._on_input_keydown} @input=${this._on_input}>`
    }
}

customElements.define('text-input', TextInput)