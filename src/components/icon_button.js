import { LitElement, html, css } from 'lit-element'
import { styleMap } from 'lit-html/directives/style-map'

import { host, link } from '../styles/common'
import { fa } from '../styles/font_awesome'

export class IconButton extends LitElement {
    static get styles() {
        return [
            host, link, fa,
            css`
            #button_container {
                position: relative;
                padding: 1em;
            }

            #button_wrapper {
                position: inherit;
                z-index: 1;
            }

            #button_wrapper a {
                margin: 0 0 0 5px;
                color: inherit;
                font-family: var(--font-monospace);
                transition: color var(--transition-duration-fast) ease-in-out;
            }

            #button_overlay {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                transition: background-color ease-in-out var(--transition-duration-fast);
            }`
        ]
    }

    static get properties() {
        return {
            icon_x: { type: Number },
            icon: { type: String },
            icon_hover: { type: String },
            colors: { type: Array },
            bg_colors: { type: Array },
            state: { type: String },
            font_em: { type: Number },
            size: { type: String }
        }
    }

    constructor() {
        super()
        this.icon = ''
        this.icon_x = 2
        this.font_em = 1
        this.colors = []
        this.bg_colors = []
        this.state = 'out'
        this.size = 'auto'
    }

    _on_mouseover(e) {
        e.stopPropagation()
        this.state = 'hover'

        this.requestUpdate()
    }

    _on_mouseout(e) {
        e.stopPropagation()
        this.state = 'out'

        this.requestUpdate()
    }


    _style_btn_container() {
        let style = { 'font-size': `${this.font_em}em` }

        if (this.size !== 'auto') {
            const sizes = this.size.split(',')
            style.width = `${sizes[0]}px`
            style.height = `${sizes[1]}px`
        }

        return styleMap(style)
    }

    _style_btn_wrapper() {
        const style = { 'color': 'inherit' }

        if (this.colors.length > 0) {
            switch (this.state) {
                case 'out':
                    style.color = `${this.colors[0]}`
                    break
                case 'hover':
                    style.color = `${this.colors[1]}`
                    break
                case 'down':
                    style.color = `${this.colors[2]}`
                    break
            }
        }

        return styleMap(style)
    }

    _style_btn_overlay() {
        const style = { 'background-color': 'inherit' }

        if (this.bg_colors.length > 0) {
            switch (this.state) {
                case 'out':
                    style['background-color'] = `${this.bg_colors[0]}`
                    break
                case 'hover':
                    style.top = '95%'
                    style['background-color'] = `${this.bg_colors[1]}`
                    break
                case 'down':
                    style.top = '95%'
                    style['background-color'] = `${this.bg_colors[2]}`
                    break
            }
        }

        return styleMap(style)
    }

    render() {
        return html`
        <div id='button_container' style='${this._style_btn_container()}'>
            <div id='button_wrapper' style='${this._style_btn_wrapper()}'>
                <i class='fa fa-${this.icon_x}x fa-${this.icon}' aria-hidden='true'></i>
                <a href='#' style='font-size:${this.icon_x}em;' @mouseover=${this._on_mouseover} @mouseout=${this._on_mouseout}>
                    <slot></slot>
                </a>
            </div>
            <div id='button_overlay' style='${this._style_btn_overlay()}'></div>
        </div>`
    }
}
customElements.define('icon-button', IconButton)