import { LitElement, html, css } from 'lit-element'

import { host, link } from '../styles/common'
import { fa } from '../styles/font_awesome'

import * as api from '../data/api'
import * as fn from '../fn'

export class GifViewer extends LitElement {
    static get styles() {
        return [
            host, link, fa,
            css`
            :host {
                font-size: 12px;
            }
        
            #background_overlay {
                display: flex;
                width: 100%;
                height: 100%;
                align-items: center;
            }

            #gif_wrapper {
                margin: 0 auto;
                padding: 10px;
                background-color: white;
                box-shadow: 0 0 5px var(--color-overlay-black-strong);
                border: 2px solid var(--color-dark);
            }

            #gif_container {
                position: relative;
                display: grid;
                justify-content: center;
                align-content: center;
                min-width:400px;

                grid-template-areas: 'frame form close';
            }

            #gif_container > a {
                grid-area: close;
                align-self: baseline;
            }

            #gif_frame {
                display: flex;
                grid-area: frame;
                background: url(../../img/transparency_bg.png) repeat;
            }

            #gif_form {
                grid-area: form;
                align-self: center;
                min-height:220px;

                color: var(--color-dark);
                margin-left: 10px;
            }

            #gif_form > h2 {
                display: inline-block;
                padding: 5px;
                margin-bottom: 10px;
                color: white;
                background-color: var(--color-dark);
            }

            a.fa-window-close-o:hover:before {
                content: '\\f2d3' !important;
            }

            #gif_form p {
                margin: 2px 0;
            }

            #tags_editor {
                margin-top: 10px;
            }

            .description-title {
                font-size: var(--font-size-large);
                text-transform: uppercase;
            }

            #delete_button {
                position: absolute;
                bottom: 10px;
                right: 10px;
            }`
        ]
    }

    static get properties() {
        return {
            gif: { type: Object }
        }
    }

    updated() {
        if (!this.gif) { return }

        const bg_overlay = this.shadowRoot.getElementById('background_overlay')
        const size = fn.constraint_to(
            [this.gif.gw_gif_width, this.gif.gw_gif_height],
            [bg_overlay.offsetWidth, bg_overlay.offsetHeight], .65)

        let width = `width: ${size[0]}px`
        let height = `height: ${size[1]}px`
        let background = `background-image: url("http://art2.network/${this.gif.gw_gif_src}")`

        if (this.gif.gw_gif_tags.find(tag => tag.name === 'favicons')) {
            width = `width: ${this.gif.gw_gif_width * 5}px`
            height = ''
            background += ';background-repeat: repeat'
        }

        this.shadowRoot.getElementById('gif_img').style = `${width};${height};${background};`
    }

    _on_delete_click(e) {
        api
            .delete_one({
                column: 'id',
                value: this.gif.gw_gif_id
            }, 'gif')
            .then((data) => {
                this.dispatchEvent(new CustomEvent('viewer-delete', {
                    detail: data
                }))
            })
    }

    _on_close_click(e) {
        e.stopPropagation()
        this._dispatch_close()
    }

    _on_overlay_click(e) {
        e.stopPropagation()
        if (e.target.getAttribute('id') === 'background_overlay') { this._dispatch_close() }
    }

    _dispatch_close() {
        this.shadowRoot.getElementById('tags_editor').setAttribute('tags', '[]')
        this.dispatchEvent(new CustomEvent('viewer-close'))
    }

    _on_tags_change(e) {
        e.stopPropagation()
        api
            .update_one({
                gif_id: this.gif.gw_gif_id,
                column: 'tags',
                value: JSON.stringify(e.detail.tag),
                subaction: e.detail.action
            }, 'gif')
            .then(() => this.dispatchEvent(new CustomEvent('tags-update', {
                bubbles: false,
                detail: this.gif.gw_gif_id
            })))
    }

    render() {
        if (!this.gif) { return false }
        return html`
        <div id='background_overlay' @click=${this._on_overlay_click}'>
            <div id='gif_wrapper'>
                <div id='gif_container'>
                    <a id='close_frame_button' href='#' class='fa fa-2x fa-window-close-o' @click=${this._on_close_click}></a>
                    <div id='gif_frame'>
                        <div id='gif_img'></div>
                    </div>
                    <div id='gif_form'>
                        <h2>${this.gif.gw_gif_title}</h2>
                        <p><span class='description-title'>size: </span>${this.gif.gw_gif_width} <i class='fa fa-times'></i> ${this.gif.gw_gif_height}</p>
                        <p><span class='description-title'>duration: </span>${this.gif.gw_gif_duration} ms</p>
                        <p><span class='description-title'>fps: </span>${(this.gif.gw_gif_frames / (this.gif.gw_gif_duration * .001)).toFixed(2)} (${this.gif.gw_gif_frames} frames)</p>
                        <tags-editor id='tags_editor' tags='${JSON.stringify(this.gif.gw_gif_tags)}' @tags-change=${this._on_tags_change}></tags-editor>
                    </div>
                    <icon-button id='delete_button' icon='trash' icon_hover='-o' icon_x='2'
                        colors='["var(--color-ultralight)","var(--color-dark)"]' bg_colors='["var(--color-accent-red)","var(--color-accent-red-light)"]' 
                        @click=${this._on_delete_click}>DELETE</icon-button>
                </div>
            </div>
        </div>`
    }
}
customElements.define('gif-viewer', GifViewer)