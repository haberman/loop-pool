import { LitElement, html, css } from 'lit-element'

import { host, list } from '../styles/common'

import * as gif from '../data/gif'

const GIF_ROOT = 'http://art2.network/works/shared/gifs'

export class GifInspector extends LitElement {
    static get styles() {
        return [
            host, list,
            css`
            :host {
                font-size: 12px;
            }
        
            li {
                display: flex;
                justify-content: center;
                align-items: center;

                overflow: hidden;
                height: var(--size-gif-height);
                border: none;
            }

            .gifs-list {
                width: calc(100% - var(--size-pannel-width) - 2em);
                margin-left: var(--size-pannel-width);
                padding: 1em;

                display: grid;
                grid-template-columns: repeat(4, 1fr);
                grid-gap: 10px;
                grid-auto-rows: minmax(var(--size-gif-height), auto);
            }

            .gif-wrapper {
                position: relative;
                display: flex;
                justify-content: center;
                align-items: center;

                overflow: hidden;
                height: var(--size-gif-height);
                border: none;

                // opacity: 0;
                background: transparent url(../../img/transparency_bg.png) repeat;
                // transition: opacity var(--transition-duration-fast) ease-in-out;
            }

            .gif-frame {
                position: absolute;
                cursor: pointer;
                top: 3px;
                left: 3px;
                bottom: 3px;
                right: 3px;
                background-color: var(--color-overlay-black);
                border: 3px solid white;

                transition: all var(--transition-duration-fast) ease-in-out;
            }

            .gif-frame:hover {
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                border-width: 0;
                background-color: transparent;
            }

            .gif-wrapper img {
                border: none;
                flex-shrink: 0;
                min-width: 100%;
                min-height: 100%;
            }`
        ]
    }

    static get properties() {
        return {
            gifs: { type: Array },
            idle: { type: Boolean }
        }
    }

    constructor() {
        super()
        this.gifs = []
        this.idle = false;
    }

    connectedCallback() {
        super.connectedCallback()

        this._binded_scroll = this._on_scroll.bind(this)
        window.addEventListener('scroll', this._binded_scroll)
    }

    insert_gifs(gifs) {
        gifs.map(gif => this.gifs.push(gif))
        this.idle = false
    }

    set_gifs(gifs) {
        this.gifs = gifs
        console.log(`new gif count ${this.gifs.length}`)
    }

    _on_gif_over(e) {
        const gif_id = e.target.parentNode.getAttribute('data-id')
        const gif_src = `http://art2.network/${gif.find_by_id(gif_id, this.gifs).gw_gif_src}`
        e.target.nextElementSibling.setAttribute('src', gif_src)
    }

    _on_gif_out(e) {
        const gif_id = e.target.parentNode.getAttribute('data-id')
        e.target.nextElementSibling.setAttribute('src', `${GIF_ROOT}/frames/${gif_id}.png`)
    }

    _on_gif_click(e) {
        const gif_id = e.target.parentNode.getAttribute('data-id')
        const index = gif.get_index_at_id(gif_id, this.gifs)
        this.dispatchEvent(new CustomEvent('gif-selected', {
            bubbles: false,
            detail: this.gifs[index]
        }))
    }

    _on_scroll(e) {
        if (this.idle) { return }
        if (window.scrollY + window.innerHeight > document.body.clientHeight - 100) {
            this.idle = true
            this.dispatchEvent(new CustomEvent('bottom-scroll', { bubbles: false }))
        }
    }

    render() {
        return html`
        <ul class='gifs-list'>
            ${this.gifs.map(gif =>
            html`
            <li>
                <div class='gif-wrapper' data-id='${gif.gw_gif_id}'>
                <div class='gif-frame' @mouseover=${this._on_gif_over} @mouseout=${this._on_gif_out} @click=${this._on_gif_click}></div>
                <img src='${GIF_ROOT}/frames/${gif.gw_gif_id}.png'>
                </div>
            </li>`)}
        </ul>`
    }
}
customElements.define('gif-inspector', GifInspector)