import { LitElement, html, css } from 'lit-element'

import { host, link, list } from '../styles/common'
import { fa } from '../styles/font_awesome'

import * as api from '../data/api'

export class SetSelector extends LitElement {
    static get styles() {
        return [
            host, link, list, fa,
            css`
            :host {
                font-size: 12px;
            }
        
            .sets-list li {
                margin: 5px 0;
                background-color: var(--color-light);
                color: white;
                transition: background-color var(--transition-duration-fast) ease-in-out;
            }

            .sets-list li.hover {
                background-color: var(--color-ultralight);
                color: black;
            }

            .sets-list li.hover-blue {
                color: var(--color-accent-blue);
            }

            .sets-list li.selected {
                background-color: white;
                color: black;
                outline: 1px solid var(--color-dark);
                box-shadow: 0 0 3px var(--color-overlay-black);
            }

            .sets-list li div {
                padding: 10px;
            }
            
            .sets-list li div p {
                font-size: 10px;
                margin: 5px 0 0 18px;
            }

            .sets-list li.selected div a {
                font-weight: bold;
            }

            .sets-list li div a:hover {
                text-decoration: underline;
            }`
        ]
    }

    static get properties() {
        return {
            sets: { type: Array },
            selected: { type: Object }
        }
    }

    constructor() {
        super()
        this.sets = []
    }

    attributeChangedCallback(name, oldval, newval) {
        if (name == 'selected') {
            const new_selected = JSON.parse(newval)

            this.shadowRoot.querySelectorAll('.set-wrapper').forEach(set_dom => {
                set_dom.parentElement.classList.remove('hover')
                if (set_dom.classList.contains('set-add')) { set_dom.parentElement.classList.remove('hover-blue') }

                if (new_selected.gw_set_id !== set_dom.getAttribute('data-id')) {
                    set_dom.parentElement.classList.remove('selected')
                } else {
                    set_dom.parentElement.classList.add('selected')
                }
            })
            this.dispatchEvent(new CustomEvent('selection-change', { detail: new_selected }))
        }

        super.attributeChangedCallback(name, oldval, newval);
    }

    _on_set_over(e) {
        const parent = e.target.parentElement

        if (parent.classList.contains('set-add')) {
            parent.parentElement.classList.add('hover', 'hover-blue')
        } else {
            parent.parentElement.classList.add('hover')
        }
    }

    _on_set_out(e) {
        const parent = e.target.parentElement

        if (parent.classList.contains('set-add')) {
            parent.parentElement.classList.remove('hover', 'hover-blue')
        } else {
            parent.parentElement.classList.remove('hover')
        }
    }

    _on_set_click(e) {
        const parent = e.target.parentElement

        if (parent.classList.contains('set-add')) {
            api
                .write_one({}, 'set ')
                .then((data) => {
                    this.sets.push(data)
                    this.setAttribute('selected', JSON.stringify(data))
                })
        } else {
            const data_id = parent.getAttribute('data-id')
            this.setAttribute('selected', JSON.stringify(this.sets.find(set => set.gw_set_id === data_id)))
        }
    }

    update_one(set_id, key, value) {
        const set_index = this.sets.findIndex(set => set.gw_set_id == set_id)
        this.sets[set_index][key] = value
        this.requestUpdate('sets')
    }

    render() {
        return html`
        <ul class='sets-list'>
            <li>
                <div class='set-add'>
                    <i class='fa fa-plus' aria-hidden='true'></i>
                    <a href='#' @click=${this._on_set_click} @mouseover=${this._on_set_over} @mouseout=${this._on_set_out}>create new set</a>
                </div>
            </li>
            ${this.sets.map(set =>
            html`
                <li>
                    <div class='set-wrapper' data-id='${set.gw_set_id}'>
                    <a href='#' @click=${this._on_set_click} @mouseover=${this._on_set_over} @mouseout=${this._on_set_out}>${set.gw_set_name}</a>
                    <p class='item-info'>${set.gw_set_info}</p>
                    </div>
                </li>
            `)}
        </ul>`
    }
}
customElements.define('set-selector', SetSelector)