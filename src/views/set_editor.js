import { LitElement, html, css } from 'lit-element'

import { host, link, list } from '../styles/common'
import { fa } from '../styles/font_awesome'
import { core, theme } from '../styles/codemirror'

import { timeOut } from '../fn'
import * as api from '../data/api'

// import CodeMirror from 'codemirror/src/codemirror'
// import jsb from 'js-beautify'

// import 'codemirror/mode/javascript'
// import 'codemirror/addon/fold/foldgutter'
// import 'codemirror/addon/fold/foldcode'
// import 'codemirror/addon/fold/brace-fold'
// import 'codemirror/addon/fold/indent-fold'
// import 'codemirror/addon/scroll/simplescrollbars'
// import 'codemirror/addon/edit/matchbrackets'
// import 'codemirror/addon/edit/closebrackets'
// import 'codemirror/addon/selection/active-line'

import '../components/text_input'
import '../components/icon_button'

export class SetEditor extends LitElement {
    static get styles() {
        return [
            host, link, list, fa, core, theme,
            css`
            :host {
                font-size: 12px;
            }

            header {
                margin: 20px;
            }

            .minimenu {
                float: right;
                font-size: 2em;
            }

            .minimenu a {
                padding: 14px;
                color: var(--color-ultralight);
                background-color: var(--color-light);
                border-radius: 100%;
                transition: color, background-color var(--transition-duration-fast) ease-in-out;
            }

            .minimenu a:hover {
                color: black;
                background-color: var(--color-ultralight);
                margin: -1px;
            }

            .minimenu a.link-badge:hover {
                color: var(--color-accent-blue);
                border: 1px solid var(--color-accent-blue);
            }

            .minimenu a.set-delete-badge:hover {
                color: var(--color-accent-red);
                border: 1px solid var(--color-accent-red);
            }

            #tracks_list {
                display: none;
            }

            #tracks_list li {
                text-align: center;
                background-color: #c5c8dc;
                border-top: 1px solid white;
                color: black;
                transition: color, background-color var(--transition-duration-fast) ease-in-out;
            }

            #tracks_list li:hover {
                cursor: pointer;
                background-color: var(--color-ultralight);
                color: var(--color-dark);
            }

            #tracks_list li.selected {
                border-color: black;
                background-color: #282a36;
                color: white;
                font-weight: bold;
            }

            #tracks_list li>span {
                display: block;
                padding: 10px;
                transition: color, background-color, border-color var(--transition-duration-fast) ease-in-out;
                overflow: hidden;
                white-space: nowrap;
            }

            #tracks_list li#track_add:hover {
                color: white;
                background-color: var(--color-accent-blue);
            }

            #input_duration {
                float: left;
                margin-right: 20px;
            }

            #delete_button {
                position: absolute;
                bottom: 10px;
                right: 16px;
                z-index: 2;
            }`
        ]
    }

    static get properties() {
        return {
            idle_time: { type: Number },
            set_data: { type: Object },
            tracks: { type: Array },
            selected_track: { type: Object }
        }
    }

    constructor() {
        super()
        this.idle_time = 1000
        this.tracks = []
    }

    connectedCallback() {
        super.connectedCallback()

        this._now = Date.now()
        this._idle_handle = null
        this._prevent_db_updates = false
        this._cursor_buffer = null
        this._scroll_buffer = null

        this._binded_idle = this._on_idle.bind(this)
        this._binded_tracks = this._on_tracks.bind(this)
    }

    firstUpdated() {
        this._editor = CodeMirror.fromTextArea(this.shadowRoot.getElementById('track_editor'), {
            mode: {
                name: 'javascript',
                json: true
            },
            theme: 'dracula',
            // scrollbarStyle: 'overlay',
            lineWrapping: true,
            lineNumbers: true,
            allowDropFileTypes: ['json'],
            foldGutter: true,
            gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
            matchBrackets: true,
            closeBrackets: true
        })

        this._editor.on('cursorActivity', cm => {
            if (this._prevent_db_updates === false) {
                this._cursor_buffer = cm.doc.getCursor()
            }
        })

        this._editor.on('scroll', () => {
            if (this._prevent_db_updates === false) {
                this._scroll_buffer = this._editor.getScrollInfo()
            }
        })

        this._editor.doc.on('change', () => {
            if (this.selected_track) {
                if (this._idle_handle !== null) {
                    timeOut.cancel(this._idle_handle)
                    this._idle_handle = null
                }
                this._idle_handle = timeOut.after(this.idle_time).run(this._binded_idle)
            }
        })
    }

    updated(changedProperties) {
        changedProperties.forEach((_oldValue, propName) => {
            switch (propName) {
                case 'set_data':
                    if (this.set_data === undefined) { break }
                    else if (!this.set_data.hasOwnProperty('gw_set_tracks') || typeof this.set_data.gw_set_tracks === 'string') {
                        api
                            .read({
                                id: this.set_data.gw_set_id
                            }, 'set')
                            .then(this._binded_tracks)
                    }
                    break

                case 'selected_track':
                    if (this.selected_track === undefined) { break }

                    this.shadowRoot.getElementById('tracks_list').querySelectorAll('li').forEach(dom_track => {
                        if (dom_track.getAttribute('data-id') === this.selected_track.gw_track_id) {
                            dom_track.classList.add('selected')
                        } else {
                            dom_track.classList.remove('selected')
                        }
                    })

                    const header_bbox = this.shadowRoot.getElementById('header').getBoundingClientRect()
                    this._editor.setSize('100%', `${window.innerHeight - header_bbox.height - 120}px`)
                    // this._editor.display.wrapper.style.display = 'block'

                    if (this.selected_track.hasOwnProperty('gw_track_id')) {
                        const selected_obj = {
                            id: this.selected_track.gw_track_id,
                            duration: this.selected_track.gw_track_duration,
                            mode: this.selected_track.gw_track_mode,
                            layers: JSON.parse(this.selected_track.gw_track_layers)
                        }
                        this._editor.setValue(js_beautify(JSON.stringify(selected_obj, null)))
                    } else {
                        this._editor.setValue('[]')
                    }

                    this._editor.focus()

                    if (this._cursor_buffer !== null) { this._editor.doc.setCursor(this._cursor_buffer) }
                    if (this._scroll_buffer !== null) { this._editor.scrollTo(this._scroll_buffer.left, this._scroll_buffer.top) }

                    this._prevent_db_updates = true
                    break
            }
        })
    }

    _track_index(track_id) {
        return this.tracks.findIndex((track) => track.gw_track_id === track_id)
    }

    _style_tracks_list() {
        let template = '50px '

        this.tracks.forEach((track) => {
            template += `${parseFloat(track.gw_track_duration)}fr `
        })

        return `display:grid;grid-template-columns:${template.substr(0, template.length - 1)};`
    }

    _on_set_delete_click() {
        api
            .delete_one({
                id: this.set_data.gw_set_id,
            }, 'set')
            .then((data) => {
                this.dispatchEvent(new CustomEvent('set-delete', {
                    detail: data
                }))
            })
    }

    _on_tracks(data) {
        if (data.gw_set_tracks.length === 1 && data.gw_set_tracks[0] === false) {
            api
                .write_one({
                    id: data.gw_set_id,
                    mode: 'set'
                }, 'track')
                .then(this._binded_tracks)
        }

        this.tracks = data.gw_set_tracks
        this.selected_track = this.tracks[0]
    }

    _on_track_click(e) {
        const selected_track_id = e.target.parentNode.getAttribute('data-id')
        const selected_track_index = this._track_index(selected_track_id)

        this.selected_track = this.tracks[selected_track_index]
        this.requestUpdate('tracks')
    }

    _on_track_add_click() {
        api
            .write_one({
                id: this.set_data.gw_set_id,
                mode: 'push'
            }, 'track')
            .then(this._binded_tracks)
    }

    _on_track_delete_click() {
        api
            .delete_one({
                set_id: this.set_data.gw_set_id,
                id: this.selected_track.gw_track_id
            }, 'track')
            .then(this._binded_tracks)
    }

    _on_set_data_update(key, value) {
        api
            .update_one({
                id: this.set_data.gw_set_id,
                column: key,
                value: value,
                mode: 'set',
            }, 'set')
            .then(data => {
                this.dispatchEvent(new CustomEvent('set-hange', {
                    detail: {
                        set: data,
                        column: key
                    }
                }))

                this.set_data = data
            })
    }

    _on_idle(_e) {
        if (this._prevent_db_updates === true) {
            this._prevent_db_updates = false
            return
        }

        this._idle_handle = null
        this._editor.setOption('readOnly', true)

        try {
            const editor_value = JSON.parse(this._editor.getValue())

            api
                .update_one({
                    id: editor_value.id,
                    mode: editor_value.mode,
                    duration: editor_value.duration,
                    layers: JSON.stringify(editor_value.layers, null)
                }, 'track')
                .then(data => {
                    const idx_track = this._track_index(data.gw_track_id)
                    this.tracks[idx_track] = data
                    this.selected_track = data
                    this._editor.setOption('readOnly', false)
                })
        } catch (e) {
            this._editor.setOption('readOnly', false)
        }
    }

    render() {
        return html`
        ${this.set_data ? html`
        <header id='header'>
            <div class='minimenu' data-id='${this.set_data.gw_set_id}'>
                <a href='http://art2.network/works/loop-player/#${this.set_data.gw_set_id}' target='_blank' class='link-badge fa fa-anchor'></a>
                <a href='#' class='set-delete-badge fa fa-times-circle' @click=${this._on_set_delete_click}></a>
            </div>
            <text-input id='input_duration' min_char='2' value='${this.set_data.gw_set_duration}'
                css='{"placeholder":{"font-size":"3em","margin":"5px 0 10px 0"},"input":{"font-size":"3em","margin":"5px -1px 10px 0"}}'
                @input-change=${e => this._on_set_data_update('duration', e.detail)}></text-input>
            <text-input id='input_name' value='${this.set_data.gw_set_name}'
                css='{"placeholder":{"font-size":"2em","margin":"0","padding":0},"input":{"font-size":"2em","margin":"0 0 -2px 0"}}'
                @input-change=${e => this._on_set_data_update('name', e.detail)}></text-input>
            <text-input id='input_info' value='${this.set_data.gw_set_info}'
                css='{"placeholder":{"font-size":"1.1em","margin":"5px 0 10px 0"},"input":{"font-size":"1.1em","margin":"5px 0 10px 0"}}'
                @input-change=${e => this._on_set_data_update('info', e.detail)}></text-input>
        </header>`: ''}
        <ul id='tracks_list' style='${this._style_tracks_list()}'>
            <li id='track_add' @click=${this._on_track_add_click}>
                <span><i class='fa fa-plus' aria-hidden='true'></i></span>
            </li>
            ${this.tracks.map(track => html`
            <li data-id='${track.gw_track_id}' @click=${this._on_track_click}>
                <span><i class='fa fa-hashtag' aria-hidden='true'></i>${track.gw_track_id}</span>
            </li>
            `)}
        </ul>
        <textarea id='track_editor'></textarea>
        ${this.set_data && this.tracks.length > 1 ? html`
        <icon-button id='delete_button' icon='trash' icon_hover='-o' icon_x='1' font_em='1.25'
            colors='["var(--color-ultralight)","white"]' bg_colors='["var(--color-accent-red)","var(--color-accent-red-light)"]'
            @click=${this._on_track_delete_click}>DELETE</icon-button>
        `: ''}`
    }
}
customElements.define('set-editor', SetEditor)