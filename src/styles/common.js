import { css } from 'lit-element'

export const host = css`
    :host {
        display: block;
        font: 10px var(--font-monospace);
        color: var(--color-dark);
    }`

export const link = css`
    a {
        text-decoration: none;
        color: var(--color-dark);
    }`

export const title = css`
    h2 {
        margin: 0;
    }

    h3 {
        padding: 1em 0 1em 1.5em;
        margin: 0 -1.5em;
    }

    h3 i[class^= "fa"] {
        margin-right: 5px;
    }`

export const list = css`
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }`

export const input = css`
    input {
        border: none;
        padding: 0;
        margin: 0;
        font-family: var(--font-monospace);
    }

    input:focus {
        outline: none;
    }`