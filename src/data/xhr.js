export const call = (url, options, progress_emitter) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const defaults = {
            method: 'GET',
            responseType: ''
        }

        let opt = options || {}

        if (url === undefined) { reject("url is missing") }

        if (opt.method === undefined) { opt.method = defaults.method }
        xhr.method = opt.method

        if (opt.responseType == undefined) { opt.responseType = defaults.responseType }
        xhr.responseType = opt.responseType

        xhr.onload = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                if (opt.responseType === '') { resolve(JSON.parse(xhr.responseText)) }
                else { resolve(xhr.response) }
            }
        }

        if (opt.method === 'GET') {
            if (opt.data !== undefined) { url += url_encode(opt.data) }

            xhr.open('GET', url)
            xhr.send()
        } else if (opt.method === 'POST') {
            xhr.open('POST', url, true)

            if (opt.data !== undefined) {
                if (opt.data instanceof FormData) {
                    if (progress_emitter !== undefined) {
                        const emitter = progress_emitter.emitter
                        const file_name = progress_emitter.file_name

                        xhr.upload.addEventListener('progress', e => {
                            const detail = { file_name: file_name, progress: e.loaded / e.total }
                            emitter.dispatchEvent(new CustomEvent('upload-progress', { detail: detail }))
                        }, false)
                    }

                    xhr.send(opt.data)
                } else {
                    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
                    xhr.send(url_encode(opt.data, 'POST'))
                }
            } else { xhr.send() }
        }
    })
}


export const url_encode = (data, method) => {
    let i, k, url, v;

    if (method == null) { method = 'GET' }

    url = method === 'GET' ? '?' : '';
    i = 0;

    for (k in data) {
        if (!data.hasOwnProperty(k)) { continue }
        v = data[k]

        if (i !== 0) { url += '&' }
        if (typeof v != 'object') { url += k + '=' + encodeURIComponent(v) }

        i++
    }

    return url
}
