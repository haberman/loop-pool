export const find_by_id = (id, arr) => {
    return arr.find(el => el.gw_gif_id === id)
}

export const get_index_at_id = (id, arr) => {
    return arr.findIndex(el => el.gw_gif_id === id)
}