import { API } from './conf';
import { call } from './xhr';

export const start = () => Promise.all([inspect('ranges'), inspect('tags'), read({}, 'sets')])

export const read = (params, edge) => {
    let data = {
        action: 'read',
        edge: edge
    }

    Object.assign(data, params)

    return call(API, {
        method: 'POST',
        data: data
    })
}

export const write_one = (params, edge) => {
    let data = {
        action: 'write',
        edge: edge
    }

    Object.assign(data, params)

    return call(API, {
        method: 'POST',
        data: data
    })
}

export const update_one = (params, edge) => {
    let data = {
        action: 'update',
        edge: edge
    }

    Object.assign(data, params)

    return call(API, {
        method: 'POST',
        data: data
    })
}

export const delete_one = (params, edge) => {
    let data = {
        action: 'delete',
        edge: edge
    }

    Object.assign(data, params)

    return call(API, {
        method: 'POST',
        data: data
    })
}

export const upload_gif = (file, progress_emitter) => {
    const data = new FormData()
    data.append('action', 'write')
    data.append('edge', 'gif')
    data.append('gif', file)

    return call(API, {
        method: 'POST',
        data: data
    }, {
            emitter: progress_emitter,
            file_name: file.name
        })
}

export const inspect = edge => {
    return call(API, {
        method: 'POST',
        data: {
            action: 'inspect',
            edge: edge
        }
    })
}
