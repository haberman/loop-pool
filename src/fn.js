export const ms_to_clock_string = ms => {
    const min = (ms % 3600000) / 60000
    const sec = ((ms % 360000) % 60000) / 1000

    return `${Math.floor(min)}:${sec.toFixed(2)}`
}

export const map_number = (n, in_min, in_max, out_min, out_max) => {
    return (n - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
}

export const constraint_to = (size_in, size_out, margin) => {
    let width = size_in[0]
    let height = size_in[1]

    if (width > height && width > size_out[0] * margin) {
        width = Math.round(size_out[0] * margin)
        height = Math.round(width * (size_in[1] / size_in[0]))
    } else if (height > width && height > size_out[1] * margin) {
        height = Math.round(size_out[1] * margin)
        width = Math.round(height * (size_in[0] / size_in[1]))
    }

    return [width, height]
}

export const timeOut = {
    /**
     * Returns a sub-module with the async interface providing the provided
     * delay.
     *
     * @memberof timeOut
     * @param {number=} delay Time to wait before calling callbacks in ms
     * @return {!AsyncInterface} An async timeout interface
     */
    after(delay) {
        return {
            run(fn) { return window.setTimeout(fn, delay) },
            cancel(handle) {
                window.clearTimeout(handle)
            }
        }
    },
    /**
     * Enqueues a function called in the next task.
     *
     * @memberof timeOut
     * @param {!Function} fn Callback to run
     * @param {number=} delay Delay in milliseconds
     * @return {number} Handle used for canceling task
     */
    run(fn, delay) {
        return window.setTimeout(fn, delay)
    },
    /**
     * Cancels a previously enqueued `timeOut` callback.
     *
     * @memberof timeOut
     * @param {number} handle Handle returned from `run` of callback to cancel
     * @return {void}
     */
    cancel(handle) {
        window.clearTimeout(handle)
    }
}